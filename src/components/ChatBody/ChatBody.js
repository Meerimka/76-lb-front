import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import '../../App.css';
import {sendMessage} from "../../store/action/messages";
import {connect} from "react-redux";

class ChatBody extends Component {

    state = {
        message: '',
        author: ''
    };

    changeHandler = (event) => {
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    sendHandler = () => {
        this.props.sendMessage(this.state.message, this.state.author)
    };

    render() {
        return (
            <Form className="Form-box" inline>
                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="Author" className="mr-sm-2">Your name </Label>
                    <Input type="text" value={this.state.author} name="author" id="Author" onChange={this.changeHandler} placeholder="enter your name" />
                </FormGroup>
                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="Message" className="mr-sm-2">Text</Label>
                    <Col sm={10}>
                        <Input type="text" value={this.state.message} name="message" id="Message" onChange={this.changeHandler} placeholder="type a text here" />
                    </Col>
                </FormGroup>
                <Button onClick={this.sendHandler}>Send</Button>
            </Form>
        )
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        sendMessage: (message, author)  => dispatch(sendMessage(message, author)),
    }};


export default connect(null,mapDispatchToProps)(ChatBody);