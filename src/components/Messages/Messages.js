import React, {Component} from 'react';
import {getMessages} from "../../store/action/messages";
import {connect} from "react-redux";

class Messages extends Component {

    componentDidMount() {
        this.props.getMessages()
    }


    componentDidUpdate(){

        clearInterval(this.interval);
        const lastDatetime = this.props.getMessages(this.props.messages.length && this.props.messages[this.props.messages.length -1].date);

        if(lastDatetime) this.interval = setInterval(this.props.getMessages, 2000, lastDatetime)
    }


    render() {
        return (
            <div>{this.props.messages.map((message, id) => {
                return (
                    <div key={id}>

                        <span>{message.author} :</span>
                        <span>{message.message} </span>
                    </div>
                )
            })
            }
            </div>
        )
    }
}
const mapStateToProps= state =>({
    messages:state.message.messages
});

const mapDispatchToProps = dispatch =>({
    getMessages: () => dispatch(getMessages())
});

export default connect(mapStateToProps,mapDispatchToProps)(Messages);