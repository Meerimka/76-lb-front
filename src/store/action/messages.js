import axios from '../../axios-messages';


export const MESSAGES_SUCCESS  = 'MESSAGES_SUCCESS';

export const getMessagesSuccess = (messages) => ({type: MESSAGES_SUCCESS,messages});


export const getMessages = ()=>{
    return dispatch =>{
        return axios.get('/messages').then(
            response =>{
                console.log(response.data);
                return dispatch(getMessagesSuccess(response.data))
            }
        )
    }
};

export const sendMessage = (message, author) =>{
    return (dispatch) => {
        const data = {
            message: message,
            author: author
        };
        console.log(data);
        axios.post('/messages', data)
            .then(
            ()=>dispatch(getMessages())
        )
    }
};

